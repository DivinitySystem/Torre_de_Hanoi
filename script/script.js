const primeiroBloco = document.createElement('li');
primeiroBloco.setAttribute('class','primeiroBloco');
const segundoBloco = document.createElement('li');
segundoBloco.setAttribute('class','segundoBloco');
const terceiroBloco = document.createElement('li');
terceiroBloco.setAttribute('class','terceiroBloco'); 
const quartoBloco = document.createElement('li');
quartoBloco.setAttribute('class','quartoBloco');
// Setando variaveis e pegando elementos. 
const start = document.getElementById('start');
const offset = document.getElementById('offset');
const end = document.getElementById('end');
const body = document.getElementById('body');
const moves = document.getElementById('moves')
const reset = document.getElementById('reset');
const victory = document.getElementById("victory")
const message = document.getElementById("message")
const exitBtn = document.getElementById("exit")
let keep = [];
let count = 0;
// Colocando os elementos na primeira torre
start.appendChild(primeiroBloco);
start.appendChild(segundoBloco);
start.appendChild(terceiroBloco);
start.appendChild(quartoBloco);
// Resetando tudo para a primeira torre
reset.addEventListener('click', () => {
    start.appendChild(primeiroBloco);
    start.appendChild(segundoBloco);
    start.appendChild(terceiroBloco);
    start.appendChild(quartoBloco);
    count = 0;
    moves.innerText = count;
    victory.innerText = '';
});
// Guardandno a posição do last element
const handlerClick = (ev) => {
  const towerId = ev.currentTarget.id;
  const towerOrigin = document.getElementById(towerId);
  const lastChild = towerOrigin.lastElementChild;
  if(keep[0] === undefined){
    if(lastChild !== null){
        keep.push(lastChild);
        keep[0].classList.add("hold");
    }    
  }
  else if(keep[0] !== undefined ){
    if(towerOrigin.lastElementChild === null){
      towerOrigin.appendChild(keep[0])
      keep[0].classList.remove("hold");
      keep.pop();
      count++;    
   }
   else if(keep[0].clientWidth < towerOrigin.lastElementChild.clientWidth){
      towerOrigin.appendChild(keep[0]);
      keep[0].classList.remove("hold");
      keep.pop();
      count++;
   }
   else if(keep[0].clientWidth > towerOrigin.lastElementChild.clientWidth){
      keep[0].classList.remove("hold");
      keep.pop();     
   }
   else if(keep[0].parentElement.id === towerId){
      keep[0].classList.remove("hold");
      keep.pop();
   }
  }
  if(end.childElementCount === 4){
    victory.innerText = `Parabéns! Você venceu com ${count} movimentos. Clique em reset para tentar novamente. =)`
    message.classList.remove("hidden");
   }
  moves.innerText = count;     
}
//Colocando o eventListener nas torres.
start.addEventListener('click',handlerClick);
offset.addEventListener('click',handlerClick);
end.addEventListener('click',handlerClick);
exitBtn.addEventListener("click", () => message.classList.add("hidden"))
